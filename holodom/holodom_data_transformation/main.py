import pandas as pd
import numpy as np
import os

# importing modules
from get_currency_exchange import run_currency_exchange
from testing_data import run_test

import warnings
warnings.filterwarnings('ignore')

PATH = f'D:/Job/git/pet_projects/holodom/'


def cleaning_1c_sales():
    df_1c_dirty = pd.read_excel(PATH + 'data/source_data/sales_1c_old_ms_dirty.xlsx')
    df_product_price = pd.read_excel(PATH + 'data/source_data/product_list_price.xlsx')

    # It can happen that Quantity < 0. Fixing error
    df_1c_dirty['Кол-во'] = df_1c_dirty['Кол-во'].apply(lambda x: -x if x < 0 else x)

    # Drop the duplicates
    df_product_price = df_product_price.drop_duplicates('Код МойСклад')

    df_1c_dirty_merged = df_1c_dirty.merge(df_product_price, left_on='Код товара', right_on='Код МойСклад') \
        [['Код товара', 'Код клиента', 'Дата', 'Ед.изм.', 'Кол-во',
          'Цена', 'Себестоимость', 'Цена продажи', 'Цена закупки', 'Номер документа']]

    # Replace the wrong purchase price
    def replace_purchase_price_1c(old_purchase_price, old_sales_price, new_purchase_price):
        if old_sales_price - old_purchase_price < 0:
            return new_purchase_price
        else:
            return old_purchase_price

    v = np.vectorize(replace_purchase_price_1c)
    df_1c_dirty_merged['new_purchase_price'] = v(df_1c_dirty_merged['Себестоимость'], df_1c_dirty_merged['Цена'],
                                                 df_1c_dirty_merged['Цена закупки'])

    # Replace the wrong sales price
    def replace_sales_price_1c(old_sales_price, new_purchase_price, new_sales_price):
        if old_sales_price - new_purchase_price < 0:
            return new_sales_price
        else:
            return old_sales_price

    v = np.vectorize(replace_sales_price_1c)
    df_1c_dirty_merged['new_sales_price'] = v(df_1c_dirty_merged['Цена'], df_1c_dirty_merged['new_purchase_price'],
                                              df_1c_dirty_merged['Цена продажи'])

    # Dropping columns
    df_1c_clean_sales = df_1c_dirty_merged.drop(['Цена', 'Себестоимость',
                                                 'Цена продажи', 'Цена закупки', 'Ед.изм.'], axis=1) \
        .rename(columns={'Дата': 'Время', 'new_purchase_price': 'Себест.',
                         'new_sales_price': 'Цена'})

    # Creating the "order_id" column
    df_1c_clean_sales['Код заказа'] = df_1c_clean_sales['Код клиента'].astype('str') + \
                                      df_1c_clean_sales['Время'].astype('str')

    df_1c_clean_sales = df_1c_clean_sales.drop('Номер документа', axis=1)

    df_1c_clean_sales.to_csv(PATH + 'data/1c_clean_sales.csv', index=False)

    return df_1c_clean_sales


def cleaning_ms_sales():
    df_ms_sales_products = pd.read_excel(PATH + 'data/source_data/ms_sales_products.xls', header=None)
    df_ms_sales_customers = pd.read_excel(PATH + 'data/source_data/ms_sales_customers.xls', header=None)
    df_ms_customers = pd.read_excel(PATH + 'data/source_data/ms_customers.xlsx', header=None)

    df_ms_sales_products.columns = df_ms_sales_products.iloc[9]
    df_ms_sales_products = df_ms_sales_products.iloc[11:]
    df_ms_sales_products = df_ms_sales_products[['Код', 'Номер', 'Время', 'Кол-во', 'Цена', 'Себест.']]
    df_ms_sales_products['join_key'] = df_ms_sales_products['Номер'].astype('str') \
                                       + df_ms_sales_products['Время'].astype('str')

    df_ms_sales_customers.columns = df_ms_sales_customers.iloc[9]
    df_ms_sales_customers = df_ms_sales_customers.iloc[11:]
    df_ms_sales_customers = df_ms_sales_customers[['Наименование', 'Номер', 'Время']]
    df_ms_sales_customers['join_key'] = df_ms_sales_customers['Номер'].astype('str') \
                                        + df_ms_sales_customers['Время'].astype('str')

    df_ms_customers.columns = df_ms_customers.iloc[0, :]
    df_ms_customers = df_ms_customers.iloc[1:, :]
    df_ms_customers_key = df_ms_sales_customers.merge(df_ms_customers, left_on='Наименование', right_on='Наименование') \
        [['Код', 'join_key']]

    df_ms_sales_products_customers = df_ms_sales_products.merge(df_ms_customers_key, on='join_key') \
        .drop('Номер', axis=1) \
        .rename(columns={'Код_x': 'Код товара', 'join_key': 'Код заказа',
                         'Код_y': 'Код клиента'})

    return df_ms_sales_products_customers


def joining_1c_ms_sales():
    df_1c_ms_dollar_sales = pd.concat([df_1c_clean_sales, df_ms_sales_products_customers])
    return df_1c_ms_dollar_sales


def saving_1c_ms_to_csv():
    df_1c_ms_dollar_sales.to_csv(PATH + 'data/1c_ms_dollar_sales.csv', index=False)


def cleaning_ms_customers():
    df_ms_customers = pd.read_excel(PATH + 'data/source_data/ms_customers.xlsx')
    df_ms_customers_clean = df_ms_customers[['Группы', 'Код', 'Наименование', 'Фактический адрес', 'Телефон',
                                             'Статус', 'Архивный', 'Комментарий']]

    df_ms_customers_clean.to_csv(PATH + 'data/ms_customers_clean.csv', index=False)


def cleaning_ms_products():
    df_ms_products = pd.read_excel(PATH + 'data/source_data/ms_products.xlsx')
    df_ms_products = df_ms_products[['Группы', 'Код', 'Наименование', 'Единица измерения', 'Поставщик', 'Архивный']]
    df_ms_products_clean = df_ms_products.dropna(subset=['Группы'])

    df_ms_products_clean['group_1'] = df_ms_products_clean['Группы'].str.split('/').str[1]
    df_ms_products_clean['group_2'] = df_ms_products_clean['Группы'].str.split('/').str[2]
    df_ms_products_clean['group_3'] = df_ms_products_clean['Группы'].str.split('/').str[3]

    df_ms_products_clean = df_ms_products_clean.drop('Группы', axis=1)

    df_ms_products_clean.to_csv(PATH + 'data/ms_products_clean.csv', index=False)


if __name__ == '__main__':
    if '1c_clean_sales.csv' in os.listdir(PATH):
        df_1c_clean_sales = pd.read_csv(PATH + 'data/1c_clean_sales.csv')
    else:
        df_1c_clean_sales = cleaning_1c_sales()

    df_ms_sales_products_customers = cleaning_ms_sales()
    df_1c_ms_dollar_sales = joining_1c_ms_sales()
    saving_1c_ms_to_csv()
    cleaning_ms_products()
    cleaning_ms_customers()

    run_currency_exchange()

    run_test()
