import pandas as pd
import requests
from bs4 import BeautifulSoup
from datetime import date
from fake_useragent import UserAgent

chrome_user_agent = UserAgent().chrome
date_from_russian = '30-12-2015'
date_to_russian = str(date.today().day) + '-' + str(date.today().month) + \
                        '-' + str(date.today().year)

url = 'https://minfin.com.ua/currency/mb/archive/usd/' \
                    + date_from_russian + '/' + date_to_russian + '/'

headers = {
        'Accept': '*/*',
        'User-Agent': UserAgent().chrome
    }


def save_page_to_html_file(link):
    req = requests.get(link, headers=headers)
    src = req.text
    with open('index.html', 'w', encoding='utf-8') as file:
        file.write(src)


def get_currency_table_from_website():
    with open('index.html', encoding='utf-8') as file:
        src = file.read()
    soup = BeautifulSoup(src, 'lxml')
    currency_table = soup.find(class_='mb-valcli--archive--table')
    return currency_table


def transform_to_df_currency_table(currency_table):
    df_currency_table = pd.DataFrame()
    for row in currency_table.find_all('tr'):
        currency_date = row.find_all('i' and 'td')[0].text
        currency_rate = row.find_all('i' and 'td')[1].text
        try:
            pd.to_datetime(currency_date, format='%d.%m.%Y').date()
        except:
            continue

        temp_df = pd.DataFrame({'Дата': pd.to_datetime(currency_date, 
                                                        format='%d.%m.%Y').date(), 
                                'Курс доллара': currency_rate}, index=[0])

        df_currency_table = pd.concat([df_currency_table, temp_df])
    df_currency_table = df_currency_table.reset_index().drop(columns={'index'})
    df_currency_table['Дата'] = df_currency_table['Дата'].astype('str')
    return df_currency_table


def create_date_table():
    date_from_us = pd.to_datetime(date_from_russian, format='%d-%m-%Y').date()
    date_to_us = pd.to_datetime(date_to_russian, format='%d-%m-%Y').date()

    df_date_table = pd.date_range(date_from_us, date_to_us)
    df_date_table = pd.DataFrame({'date': df_date_table})
    df_date_table['date'] = df_date_table['date'].astype('str')
    return df_date_table


def combine_date_and_currency_table(df_date_table, df_currency_table):
    df_merged_table = df_date_table.merge(df_currency_table, left_on='date', right_on='Дата', how='left')
    df_merged_table['Курс доллара'] = df_merged_table['Курс доллара'].fillna(method='ffill')
    df_merged_table['Курс доллара'] = df_merged_table['Курс доллара'].fillna(method='bfill')
    df_merged_table = df_merged_table.drop('Дата', axis=1)
    df_merged_table.to_csv('D:/Job/git/pet_projects/holodom/data/exchange_rates.csv', index=False)


def run_currency_exchange():
    save_page_to_html_file(url)
    currency_table_dirty = get_currency_table_from_website()
    df_currency_table = transform_to_df_currency_table(currency_table_dirty)
    df_date_table = create_date_table()
    combine_date_and_currency_table(df_date_table, df_currency_table)



