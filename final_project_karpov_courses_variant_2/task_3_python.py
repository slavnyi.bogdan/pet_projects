import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os


def create_data_files():
    data_files = ['customers.csv', 'products.csv', 'sales.csv']
    files_count = 0

    for filename in data_files:
        if filename in os.listdir():
            files_count += 1

    if files_count != len(data_files):
        # Customer table
        n_customers = 120

        customer_id_list = np.arange(0, n_customers)
        city_list = ['Moscow', 'Peterburg', 'Rostov', 'Tver']
        customer_city = np.random.choice(city_list, size=n_customers, p=[0.5, 0.3, 0.15, 0.05])

        df_customers = pd.DataFrame({'customer_id': customer_id_list,
                                    'customer_city': customer_city})
        df_customers.to_csv('customers.csv', index=False)

        # Product table
        n_products = 250

        product_id_list = np.arange(0, n_products)
        product_price_list = np.random.choice(np.arange(250, 550), size=n_products) 

        df_products = pd.DataFrame({'product_id': product_id_list,
                                   'product_price': product_price_list})

        df_products.to_csv('products.csv', index=False)

        # Sales table
        table_size = 50000
        start_date = '2017/01/01'
        end_date = '2020/12/31'

        random_date = pd.Series(np.random.choice(pd.date_range(start_date, end_date),
                                         size=table_size)).astype('str')
        date_column = pd.to_datetime(random_date)

        customer_id_list_column = np.random.choice(df_customers['customer_id'], size=table_size)

        product_id_list_column = np.random.choice(df_products['product_id'], size=table_size)

        quantity_column = np.random.choice(np.arange(1, 5), size=table_size)

        df_sales = pd.DataFrame({'sale_date': date_column,
                                'customer_id': customer_id_list_column,
                                'product_id': product_id_list_column,
                                'quantity': quantity_column})

        df_sales.to_csv('sales.csv', index=False)
        print('".csv" data files are created!')
    else:
        print('".csv" data files already exist!')


def load_transform_data():
    df_customers = pd.read_csv('customers.csv')
    df_products = pd.read_csv('products.csv')
    df_sales = pd.read_csv('sales.csv')
    df_sales_full = df_sales.merge(df_products, on='product_id').merge(df_customers, on='customer_id')
    df_sales_full['total_sales'] = df_sales_full['quantity'] * df_sales_full['product_price']
    df_sales_full['sale_date'] = pd.to_datetime(df_sales_full['sale_date'])
    df_sales_full['year_month'] = df_sales_full['sale_date'].dt.year.astype('str') + \
                    df_sales_full['sale_date'].dt.month.apply(lambda x: '0'+str(x) if len(str(x)) < 2 else str(x))
    df_sales_full['year'] = df_sales_full['sale_date'].dt.year
    df_sales_full['check_id'] = df_sales_full['sale_date'].astype('str') + df_sales_full['customer_id'].astype('str')
    print('Full dataframe is created!')
    return df_sales_full


def build_year_month_sale_graph(df_sales_full):
    plt.figure(figsize=(12, 5))
    grp_year_month_sales = df_sales_full.groupby('year_month', as_index=False) \
                                        .agg({'total_sales': 'sum'})
    sns.lineplot(data=grp_year_month_sales, x='year_month', y='total_sales')
    plt.xticks(rotation=90)
    plt.ylim(0)
    plt.title('Year month sales')
    plt.show()

    
def build_monthly_avg_check_graph(df_sales_full):
    plt.figure(figsize=(12, 5))

    grp_monthly_avg_check = df_sales_full.groupby(['check_id', 'year_month'], as_index=False) \
                                        .agg({'total_sales': 'sum'}) \
                                        .groupby('year_month', as_index=False) \
                                        .agg({'total_sales': 'mean'}) \
                                        .rename(columns={'total_sales': 'monthly_avg_check'})
    sns.lineplot(data=grp_monthly_avg_check, x='year_month', y='monthly_avg_check')
    plt.xticks(rotation=90)
    plt.ylim(0)
    plt.title('Monthly avg. check')
    plt.show()
    
    
def build_monthly_checks_count_graph(df_sales_full):
    plt.figure(figsize=(12, 5))

    grp_monthly_check_count = df_sales_full.groupby(['year_month', 'check_id'], as_index=False) \
                                            .agg({'product_id': 'count'}) \
                                            .groupby('year_month', as_index=False) \
                                            .agg({'check_id': 'count'})
    sns.lineplot(data=grp_monthly_check_count, x='year_month', y='check_id')
    plt.xticks(rotation=90)
    plt.ylim(0)
    plt.xlabel('check_count')
    plt.title('Monthly checks count')
    plt.show()
    
    
def build_total_sales_by_customer_city_graph(df_sales_full):
    grp_yearly_city_sales = df_sales_full.groupby(['year', 'customer_city'], as_index=False) \
                                    .agg({'total_sales': 'sum'})
    sns.barplot(data=grp_yearly_city_sales , x='year', y='total_sales', hue='customer_city')
    plt.title('Total sales by customer and year')
    plt.show()
    
    
def build_avg_check_distribution_graph(df_sales_full):
    grp_avg_check_distribution = df_sales_full.groupby('check_id', as_index=False) \
                                    .agg({'total_sales': 'sum'})
    sns.histplot(data=grp_avg_check_distribution, x='total_sales')
    plt.xlabel('total_sales')
    plt.ylabel('quantity')
    plt.title('Avg check distribution')
    plt.show()
    
    
def build_customer_total_sales_distribution_graph(df_sales_full):
    grp_customer_total_sales_distribution = df_sales_full.groupby('customer_id', as_index=False) \
                                                .agg({'total_sales': 'sum'})
    sns.histplot(data=grp_customer_total_sales_distribution, x='total_sales')
    plt.title('Customer total sales distribution')
    plt.xlabel('customer_total_sales')
    plt.ylabel('customer_count')
    plt.show()
    


def choose_and_build_graph(df_sales_full, name_of_graph='year_month_sales'):
    """
    Parameters
    ------------
    name_of_graph: str, default='year_month_sales'
    There are also graphs as 
    ['monthly_avg_check', 'monthly_checks_count', 'total_sales_by_customer_city',
    'avg_check_distribution', 'customer_total_sales_distribution']
    """
    if name_of_graph == 'year_month_sales':
        build_year_month_sale_graph(df_sales_full)
        
    if name_of_graph == 'monthly_avg_check':
        build_monthly_avg_check_graph(df_sales_full)
        
    if name_of_graph == 'monthly_checks_count':
        build_monthly_checks_count_graph(df_sales_full)
        
    if name_of_graph == 'total_sales_by_customer_city':
        build_total_sales_by_customer_city_graph(df_sales_full)
        
    if name_of_graph == 'avg_check_distribution':
        build_avg_check_distribution_graph(df_sales_full)
        
    if name_of_graph == 'customer_total_sales_distribution':
        build_customer_total_sales_distribution_graph(df_sales_full)
    
    
def main():
   
    create_data_files()
    try:
        df_sales_full.shape[0]
    except Exception as ex:
        df_sales_full = load_transform_data()
        
    choose_and_build_graph(df_sales_full, name_of_graph='avg_check_distribution')
    
    
main()